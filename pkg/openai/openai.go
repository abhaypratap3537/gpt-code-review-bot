package chat

import (
	"context"
	"fmt"
	"strings"
	"time"

	openai "github.com/sashabaranov/go-openai"
)

//var MaxPatchCount = 4000

type CompletionConfig struct {
	MaxTokens        int            `json:"max_tokens,omitempty"`
	Temperature      float32        `json:"temperature,omitempty"`
	TopP             float32        `json:"top_p,omitempty"`
	N                int            `json:"n,omitempty"`
	Stream           bool           `json:"stream,omitempty"`
	LogProbs         int            `json:"logprobs,omitempty"`
	Echo             bool           `json:"echo,omitempty"`
	Stop             []string       `json:"stop,omitempty"`
	PresencePenalty  float32        `json:"presence_penalty,omitempty"`
	FrequencyPenalty float32        `json:"frequency_penalty,omitempty"`
	BestOf           int            `json:"best_of,omitempty"`
	LogitBias        map[string]int `json:"logit_bias,omitempty"`
}

type Chat struct {
	Client           *openai.Client
	CompletionConfig *CompletionConfig
}

func NewChat(apiKey string, completionConfig *CompletionConfig) *Chat {
	fmt.Println("[access to openai]")
	client := openai.NewClient(apiKey)
	return &Chat{
		Client:           client,
		CompletionConfig: completionConfig,
	}
}

func (c *Chat) SendMessage(prompt string) (string, error) {
	fmt.Println("[request to openai]")
	resp, err := c.Client.CreateChatCompletion(
		context.Background(),
		openai.ChatCompletionRequest{
			Model: openai.GPT3Dot5Turbo,
			Messages: []openai.ChatCompletionMessage{
				{
					Role:    openai.ChatMessageRoleUser,
					Content: prompt,
				},
			},
			MaxTokens:        c.CompletionConfig.MaxTokens,
			Temperature:      c.CompletionConfig.Temperature,
			TopP:             c.CompletionConfig.TopP,
			N:                c.CompletionConfig.N,
			Stream:           c.CompletionConfig.Stream,
			Stop:             c.CompletionConfig.Stop,
			PresencePenalty:  c.CompletionConfig.PresencePenalty,
			FrequencyPenalty: c.CompletionConfig.FrequencyPenalty,
			LogitBias:        c.CompletionConfig.LogitBias,
		},
	)
	if err != nil {
		return "", err
	}

	return resp.Choices[0].Message.Content, nil
}

func (c *Chat) CodeReview(prompt string) (string, error) {
	if prompt == "" {
		return ":warning: diff code is empty", nil
	}
	start := time.Now()
	tokenNum := len(strings.ReplaceAll(prompt, " ", ""))
	fmt.Println("token: ", tokenNum)

	res, err := c.SendMessage(prompt)
	if err != nil {
		return "", err
	}
	elapsed := time.Since(start)
	fmt.Printf("code-review cost: %v\n", elapsed)
	return res, nil
}
