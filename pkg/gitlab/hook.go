package gitlab

type ChangePosition struct {
	BaseSha      string    `json:"base_sha"`
	StartSha     string    `json:"start_sha"`
	HeadSha      string    `json:"head_sha"`
	OldPath      string    `json:"old_path"`
	NewPath      string    `json:"new_path"`
	PositionType string    `json:"position_type"`
	OldLine      string    `json:"old_line"`
	NewLine      string    `json:"new_line"`
	LineRange    LineRange `json:"line_range"`
}

type LinePosition struct {
	LineCode string `json:"line_code"`
	Type     string `json:"type"`
	OldLine  int    `json:"old_line"`
	NewLine  int    `json:"new_line"`
}

type LineRange struct {
	Start LinePosition `json:"start"`
	End   LinePosition `json:"end"`
}

type OriginalPosition struct {
	BaseSha      string    `json:"base_sha"`
	StartSha     string    `json:"start_sha"`
	HeadSha      string    `json:"head_sha"`
	OldPath      string    `json:"old_path"`
	NewPath      string    `json:"new_path"`
	PositionType string    `json:"position_type"`
	OldLine      int       `json:"old_line"`
	NewLine      int       `json:"new_line"`
	LineRange    LineRange `json:"line_range"`
}

type Position struct {
	BaseSha      string    `json:"base_sha"`
	StartSha     string    `json:"start_sha"`
	HeadSha      string    `json:"head_sha"`
	OldPath      string    `json:"old_path"`
	NewPath      string    `json:"new_path"`
	PositionType string    `json:"position_type"`
	OldLine      int       `json:"old_line"`
	NewLine      int       `json:"new_line"`
	LineRange    LineRange `json:"line_range"`
}

type User struct {
	Id        int    `json:"id"`
	Name      string `json:"name"`
	Username  string `json:"username"`
	AvatarUrl string `json:"avatar_url"`
	Email     string `json:"email"`
}

type Project struct {
	Id                int    `json:"id"`
	Name              string `json:"name"`
	Description       string `json:"description"`
	WebUrl            string `json:"web_url"`
	AvatarUrl         string `json:"avatar_url"`
	GitSshUrl         string `json:"git_ssh_url"`
	GitHttpUrl        string `json:"git_http_url"`
	Namespace         string `json:"namespace"`
	VisibilityLevel   int    `json:"visibility_level"`
	PathWithNamespace string `json:"path_with_namespace"`
	DefaultBranch     string `json:"default_branch"`
	CiConfigPath      string `json:"ci_config_path"`
	Homepage          string `json:"homepage"`
	Url               string `json:"url"`
	SshUrl            string `json:"ssh_url"`
	HttpUrl           string `json:"http_url"`
}

type ObjectAttributes struct {
	Attachment       string           `json:"attachment"`
	AuthorId         int              `json:"author_id"`
	ChangePosition   ChangePosition   `json:"change_position"`
	CommitId         string           `json:"commit_id"`
	CreatedAt        interface{}      `json:"created_at"`
	DiscussionId     string           `json:"discussion_id"`
	Id               int              `json:"id"`
	LineCode         string           `json:"line_code"`
	Note             string           `json:"note"`
	NoteableId       int              `json:"noteable_id"`
	NoteableType     string           `json:"noteable_type"`
	OriginalPosition OriginalPosition `json:"original_position"`
	Position         Position         `json:"position"`
	ProjectId        int              `json:"project_id"`
	ResolvedAt       string           `json:"resolved_at"`
	ResolvedById     string           `json:"resolved_by_id"`
	ResolvedByPush   string           `json:"resolved_by_push"`
	StDiff           string           `json:"st_diff"`
	System           bool             `json:"system"`
	Type             string           `json:"type"`
	UpdatedAt        interface{}      `json:"updated_at"`
	UpdatedById      string           `json:"updated_by_id"`
	Description      string           `json:"description"`
	Url              string           `json:"url"`
}

type Repository struct {
	Name        string `json:"name"`
	Url         string `json:"url"`
	Description string `json:"description"`
	Homepage    string `json:"homepage"`
}

type MergeParams struct {
	ForceRemoveSourceBranch  interface{} `json:"force_remove_source_branch"`
	ShouldRemoveSourceBranch bool        `json:"should_remove_source_branch"`
}

type Source struct {
	Id                int    `json:"id"`
	Name              string `json:"name"`
	Description       string `json:"description"`
	WebUrl            string `json:"web_url"`
	AvatarUrl         string `json:"avatar_url"`
	GitSshUrl         string `json:"git_ssh_url"`
	GitHttpUrl        string `json:"git_http_url"`
	Namespace         string `json:"namespace"`
	VisibilityLevel   int    `json:"visibility_level"`
	PathWithNamespace string `json:"path_with_namespace"`
	DefaultBranch     string `json:"default_branch"`
	CiConfigPath      string `json:"ci_config_path"`
	Homepage          string `json:"homepage"`
	Url               string `json:"url"`
	SshUrl            string `json:"ssh_url"`
	HttpUrl           string `json:"http_url"`
}

type Target struct {
	Id                int    `json:"id"`
	Name              string `json:"name"`
	Description       string `json:"description"`
	WebUrl            string `json:"web_url"`
	AvatarUrl         string `json:"avatar_url"`
	GitSshUrl         string `json:"git_ssh_url"`
	GitHttpUrl        string `json:"git_http_url"`
	Namespace         string `json:"namespace"`
	VisibilityLevel   int    `json:"visibility_level"`
	PathWithNamespace string `json:"path_with_namespace"`
	DefaultBranch     string `json:"default_branch"`
	CiConfigPath      string `json:"ci_config_path"`
	Homepage          string `json:"homepage"`
	Url               string `json:"url"`
	SshUrl            string `json:"ssh_url"`
	HttpUrl           string `json:"http_url"`
}

type Author struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

type LastCommit struct {
	Id        string      `json:"id"`
	Message   string      `json:"message"`
	Title     string      `json:"title"`
	Timestamp interface{} `json:"timestamp"`
	Url       string      `json:"url"`
	Author    Author      `json:"author"`
}

type MergeRequest struct {
	AssigneeId                  int         `json:"assignee_id"`
	AuthorId                    int         `json:"author_id"`
	CreatedAt                   interface{} `json:"created_at"`
	Description                 string      `json:"description"`
	HeadPipelineId              int         `json:"head_pipeline_id"`
	Id                          int         `json:"id"`
	Iid                         int         `json:"iid"`
	LastEditedAt                string      `json:"last_edited_at"`
	MergeCommitSha              string      `json:"merge_commit_sha"`
	MergeError                  string      `json:"merge_error"`
	MergeParams                 MergeParams `json:"merge_params"`
	MergeStatus                 string      `json:"merge_status"`
	MergeUserId                 string      `json:"merge_user_id"`
	MergeWhenPipelineSucceeds   bool        `json:"merge_when_pipeline_succeeds"`
	MilestoneId                 string      `json:"milestone_id"`
	SourceBranch                string      `json:"source_branch"`
	SourceProjectId             int         `json:"source_project_id"`
	StateId                     int         `json:"state_id"`
	TargetBranch                string      `json:"target_branch"`
	TargetProjectId             int         `json:"target_project_id"`
	TimeEstimate                int         `json:"time_estimate"`
	Title                       string      `json:"title"`
	UpdatedAt                   interface{} `json:"updated_at"`
	Url                         string      `json:"url"`
	Source                      Source      `json:"source"`
	Target                      Target      `json:"target"`
	LastCommit                  LastCommit  `json:"last_commit"`
	WorkInProgress              bool        `json:"work_in_progress"`
	TotalTimeSpent              int         `json:"total_time_spent"`
	TimeChange                  int         `json:"time_change"`
	HumanTotalTimeSpent         string      `json:"human_total_time_spent"`
	HumanTimeChange             string      `json:"human_time_change"`
	HumanTimeEstimate           string      `json:"human_time_estimate"`
	AssigneeIds                 []int       `json:"assignee_ids"`
	ReviewerIds                 []int       `json:"reviewer_ids"`
	Labels                      *[]Label    `json:"labels"`
	State                       string      `json:"state"`
	BlockingDiscussionsResolved bool        `json:"blocking_discussions_resolved"`
	FirstContribution           bool        `json:"first_contribution"`
	DetailedMergeStatus         string      `json:"detailed_merge_status"`
}

type Label struct {
	Id          int    `json:"id"`
	Title       string `json:"title"`
	Color       string `json:"color"`
	ProjectId   string `json:"project_id"`
	CreatedAt   any    `json:"created_at"`
	UpdatedAt   any    `json:"updated_at"`
	Template    bool   `json:"template"`
	Description string `json:"description"`
	Type        string `json:"type"`
	GroupId     int    `json:"group_id"`
}

type NoteHook struct {
	ObjectKind       string           `json:"object_kind"`
	EventType        string           `json:"event_type"`
	User             User             `json:"user"`
	ProjectId        int              `json:"project_id"`
	Project          Project          `json:"project"`
	ObjectAttributes ObjectAttributes `json:"object_attributes"`
	Repository       Repository       `json:"repository"`
	MergeRequest     MergeRequest     `json:"merge_request"`
}
