# GPT Code Review Bot

An automated code review bot for GitLab MR based on GitLab webhook + GitLab CI + ChatGPT/Azure OpenAi.

## 准备

- GitLab账号
- [OpenAI](https://platform.openai.com/account/api-keys) 账号 或 Azure OpenAI账号

## 操作步骤

### 1. 创建 GitLab 项目

在可访问OpenAI API或Azure OpenAI的GitLab 实例上新建一个空白项目。

这个项目用于运行 bot ，不是你要进行 code review 的项目。

### 2. 配置 Pipeline Trigger

Settings -> CI/CD -> Pipeline triggers -> Add trigger -> Copy Token

获取 Trigger Token 用于触发 CI。

### 3. 获取 GitLab Access Token

在需要进行 code review 的项目生成一个 Access Token ，或创建群组Access Token、Personal Access Token，用于调用 GitLab API。

需要勾选范围 api。

### 4. 配置 CI/CD Variables

在运行 bot 的项目中创建CI/CD环境变量，勾选隐藏变量：
- OpenAI
  - GITLAB_TOKEN：上一步获取的GitLab Access Token
  - OPENAI_API_KEY：从OpenAI获取的API Token
- Azure OpenAI
  - GITLAB_TOKEN：上一步获取的GitLab Access Token
  - AZURE_API_KEY：从Azure OpenAI获取的API密钥
  - AZURE_RESOURCE_NAME：Azure OpenAI的EndPoint前缀，如https://AZURE_RESOURCE_NAME.openai.azure.com
  - AZURE_MODEL_TYPE: 可选。Azure OpenAI模型类型，如`gpt-3.5-turbo-0301`、`gpt-3.5-turbo`。默认是`gpt-3.5-turbo`
  - AZURE_DEPLOYMENT_NAME：Azure OpenAI模型名称
  - AZURE_API_VERSION：可选。Azure OpenAI的API版本，如`2023-03-15-preview`、`2022-12-01`。默认是`2023-03-15-preview`
  ![](docs/images/variable.png)


### 5. 配置 `.gitlab-ci.yml`

创建 `.gitlab-ci.yml` 并提交以下内容：

```yaml
stages:
  - job

job:
  image: sonicrang/gpt_code_review_bot:20230615a
  stage: job
  variables:
    API_TYPE: openai # 或azure
    GITLAB_HOST: "https://jihulab.com" # gitlab url
    LANGUAGE: Chinese # 语言，默认为English
  script:
    - app
  only:
    - trigger
```

### 6. 配置 Webhook

注意：这里配置的 Webhook 是你需要 code review 的项目。

Settings -> Webhooks -> 勾选 Comments

在 URL 中插入： `https://GITLAB_HOST/api/v4/projects/PROJECT_ID/ref/REF_NAME/trigger/pipeline?token=TOKEN`

其中：

- `GITLAB_HOST` bot项目所在 GitLab 实例的域名，如为 https://gitlab.com
- `PROJECT_ID`: bot项目ID
- `REF_NAME`: 分支名称
- `TOKEN`: 触发令牌，请将上一步中生成的 token 复制到这里

最后点击 `Add Webhook`

## 使用

目前支持 3 中触发方式

### Review MR 中所有修改的文件

在 MR 中评论 `/review-all` 即可触发。

### Review 单个 Diff 文件

点击 `变更`，在具体需要 Review 的文件上的任意行发送评论，内容为 `/review` 即可触发。

### Review 单个文件并替换命令评论

操作方式如上，命令改为 `/review-r`，这样 code review 的内容会替换掉有 `/review-r` 的评论。

## 可定制化配置

### 自定义 Prompt

自定义 Prompt
默认的 Prompt 为：
> Review the below-mentioned code and provide some suggestions in %s language with regards to better readability, 
performance, and security, or any other recommends. If there are any recommendation, kindly provide the sample code. 
For the code part, please use two ``` wrappers. In the following, + represents a new line of code and - a reduced line of code.\n

这里的 %s 会被 LANGUAGE 变量替换，如 LANGUAGE 为 Chinese，自定义时需要注意。
可以通过 variables 中的 PROMPT 来自定义，如：

```
variables:
  GITLAB_HOST: "https://jihulab.com"
  LANGUAGE: Chinese
  PROMPT: "Please write in %s language and ... "
```


### 设置 Completion 参数

可以使用在 CI/CD Variables 中配置 key 为 COMPLETION_CONFIG 类型为 file 的 Variables 来给 Completion 注入参数，参数解释详见 https://platform.openai.com/docs/api-reference/completions

![](docs/images/completion.png)

如果使用Azure OpenAI，建议将max_tokens设置大一些，如4000，否则返回内容会比较少
```
{
  "max_tokens": 4000,
  "temperature": 0.8,
  "top_p": 0,
  "n": 0,
  "stream": false,
  "logprobs": 0,
  "echo": false,
  "stop": null,
  "presence_penalty": 0,
  "frequency_penalty": 0,
  "best_of": 0,
  "logit_bias": null
}

```
